Binary stream parsing using Akka and ReactiveX
----------------------------------------------

This is done in class MatchEvent, using a set of bit masks and right shifting to extract individual fields.

MatchEventSpec shows usage of the parser and proves the logic for some example cases.

Checking and fixing events
--------------------------

The scheme I adopted is fairly simplistic but hopefully provides a framework into which would fit a more sophisticated approach.

As each match update is received, MatchState invokes Fixes.clean on the collection of MatchEvents thus far.

Fixes.clean applies a set of mutating fixes to the events (plus the identity operation) and logs
the number of errors detected against each fix.

Finally, it sorts the fix results by number of errors and returns the mutation that gives the lowest
error count. In essence, it is a very simple optimizer, that minimizes the data stream errors.

Error counting itself is handled by class Checks.

Sample app
----------

There is a sample app that highlights usage of akka and the reactive extensions library from reactivex.io.

I used ReactiveX for creating observable streams of data, representing the wire feed. Each new item is
passed to a MatchStateActor, which updates the game state.

There is also a separate stream of game-state queries which are passed to the same MatchStateActor.
 
The point of the actor here is that the game updates and queries happen at independent intervals,
so akka's messaging provides the required synchronization and consistency of state.

The application creates a sped-up simulation of a game, with a game event about 500ms
and a query about the game's state every 550ms.

Running the sample
------------------

sbt "run-main akka.Main TickerTapeMain"