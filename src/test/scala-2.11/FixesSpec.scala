import Fixes._
import Checks._

class FixesSpec extends UnitTest {

  "A valid sequence of events" should "not be modified" in {
     clean(TestData.sample1Events.reverse) shouldBe TestData.sample1Events.reverse
  }

  "An invalid sequence of events" should "be improved somewhat" in {
    val events = TestData.sample2Events.reverse

    val n1 = errorsIn(events)

    val cleanedEvents = clean(events)

    val n2 = errorsIn(cleanedEvents)

    //println(s"n1=$n1, n2=$n2")
    //println(cleanedEvents)
    n2 shouldBe <= (n1)
  }

  "A sequence with time misordering" should "be reordered" in {
    clean(
      List(
        MatchEvent(2,1,2,2,28),
        MatchEvent(3,1,5,2,60),
        MatchEvent(2,0,0,2,15))) shouldBe
      List(
        MatchEvent(3,1,5,2,60),
        MatchEvent(2,1,2,2,28),
        MatchEvent(2,0,0,2,15))
  }
}
