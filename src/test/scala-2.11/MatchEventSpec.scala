

class MatchEventSpec extends UnitTest {

  "The value 0x781002" should "represent Team 1 scoring 2 points after 15 seconds" in {
    val event: MatchEvent = MatchEvent(points = 2, team = 0, team2Total = 0, team1Total = 2, timeSecs = 15)
    MatchEvent.parseInt(0x781002) shouldBe event
    MatchEvent.toInt(event) shouldBe 0x781002
  }

  "The value 0xf0101f" should "represent Team 2 scoring 3 points after 30 seconds" in {
    val event: MatchEvent = MatchEvent(points = 3, team = 1, team2Total = 3, team1Total = 2, timeSecs = 30)
    MatchEvent.parseInt(0xf0101f) shouldBe event
    MatchEvent.toInt(event) shouldBe 0xf0101f
  }

  "0x1310c8a1" should "represent - At 10:10, a single point for Team 1 gives them a 5 point lead – 25-20" in {
    val event: MatchEvent = MatchEvent(points = 1, team = 0, team2Total = 20, team1Total = 25, timeSecs = 610)
    MatchEvent.parseInt(0x1310c8a1) shouldBe event
    MatchEvent.toInt(event) shouldBe 0x1310c8a1
  }

  "0x29f981a2" should "represent - At 22:23, a 2-point shot for Team 1 leaves them 4 points behind at 48-52" in {
    val event: MatchEvent = MatchEvent(points = 2, team = 0, team2Total = 52, team1Total = 48, timeSecs = 1343)
    MatchEvent.parseInt(0x29f981a2) shouldBe event
    MatchEvent.toInt(event) shouldBe 0x29f981a2
  }

  "0x48332327" should "represent - At 38:30, a 3-point shot levels the game for Team 2 at 100 points each" in {
    val event: MatchEvent = MatchEvent(points = 3, team = 1, team2Total = 100, team1Total = 100, timeSecs = 2310)
    MatchEvent.parseInt(0x48332327) shouldBe event
    MatchEvent.toInt(event) shouldBe 0x48332327
  }


}
