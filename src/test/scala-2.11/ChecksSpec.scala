
import Checks._

class ChecksSpec extends UnitTest {

  "An event with a valid team" should "pass the valid team check" in {
    validTeam(MatchEvent(2, 0, 0, 2, 10), None) shouldBe true
    validTeam(MatchEvent(2, 1, 2, 0, 10), None) shouldBe true
  }

  "An event with an invalid team" should "fail the valid team check" in {
    validTeam(MatchEvent(2, 2, 0, 2, 10), None) shouldBe false
  }

  "An event with correct initial totals" should "pass the initial total check" in {
    initialTotalAddsUp(MatchEvent(2, 0, 0, 2, 10), None) shouldBe true
    initialTotalAddsUp(MatchEvent(2, 1, 2, 0, 10), None) shouldBe true
  }

  "An event where initial totals are wrongly assigned" should "fail the initial total check" in {
    initialTotalAddsUp(MatchEvent(2, 0, 2, 0, 10), None) shouldBe false
    initialTotalAddsUp(MatchEvent(2, 1, 0, 2, 10), None) shouldBe false
  }

  "Some events where the totals add up correctly" should "pass the totals check" in {
    totalsAddUp(MatchEvent(2,1,7,4,92), Some(MatchEvent(2,0,5,4,73))) shouldBe true
  }

  "Some events where the totals do not add up" should "fail the totals check" in {
    totalsAddUp(MatchEvent(2,0,5,4,73), Some(MatchEvent(2,1,8,4,92))) shouldBe false
  }

  "Some events with a valid time sequence" should "pass the time sequence check" in {
    validTimeSequence(MatchEvent(2,1,8,4,92), Some(MatchEvent(2,0,5,4,73))) shouldBe true
  }

  "Some events with an invalid time sequence" should "fail the time sequence check" in {
    validTimeSequence(MatchEvent(2,0,5,4,73), Some(MatchEvent(2,1,8,4,92))) shouldBe false
  }

  "The series1 test data" should "generate zero check errors" in {
    errorsIn(TestData.sample1Events.reverse) shouldBe 0
  }

  "The series2 test data" should "generate check errors" in {
    //TestData.sample2Events.reverse.foreach(println)
    errorsIn(TestData.sample2Events.reverse) shouldBe >= (0)
  }

}
