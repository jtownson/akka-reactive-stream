import scala.annotation.tailrec
import Checks._

import Checks._

object Fixes {

  type Fix = List[MatchEvent] => List[MatchEvent]

  val removeRedundantEntries: Fix = events => {
    events.filter(event => validTeam(event, None))
  }

  val fixTeam1Score: Fix = events => events

  val reorderTimeSequence: Fix = events => events.sortBy(event => event.timeSecs).reverse

  val fixes: List[Fix] = List(identity, removeRedundantEntries, fixTeam1Score, reorderTimeSequence)

  def clean(events: List[MatchEvent]): List[MatchEvent] = {

    @tailrec
    def loop(events: List[MatchEvent], iFix: Int, acc: List[List[MatchEvent]]): List[List[MatchEvent]] = {

      val fix: List[MatchEvent] = fixes(iFix)(events)

      val errors: Int = errorsIn(fix)

      if (errors > 0 && iFix < fixes.size - 1) {
        loop(events, iFix + 1, fix :: acc)
      } else {
        fix :: acc
      }
    }

    val solutions = loop(events, iFix = 0, acc = List())

    val sortSolutions: List[List[MatchEvent]] = solutions.sortBy(_.size)

    // return solution with least errors
    solutions.sortBy(_.size).head
  }
}
