import scala.annotation.tailrec

object Checks {

  type Check = (MatchEvent, Option[MatchEvent]) => Boolean

  val validTeam: Check = (event, previousEvent) => event.team == 0 || event.team == 1

  val initialTotalAddsUp: Check = (event, previousEvent) => {
    val b = teamTotal(event, event.team) == event.points &&
    teamTotal(event, otherTeam(event.team)) == 0
    report(b, "initialTotalAddsUp failed", event, previousEvent)
  }

  val totalsAddUp: Check = (event, previousEvent) => previousEvent match {
    case Some(previous) =>
      val scoringTeam = event.team
      val nonScoringTeam = otherTeam(scoringTeam)
      val b = teamTotal(previous, scoringTeam) + teamPoints(event, scoringTeam) == teamTotal(event, scoringTeam) &&
              teamTotal(previous, nonScoringTeam) + 0 == teamTotal(event, nonScoringTeam)
      report(b, "totalsAddUp failed", event, previousEvent)
    case None => initialTotalAddsUp(event, None)
  }

  val validTimeSequence: Check = (event, previousEvent) => previousEvent match {
    case Some(previous) => {
      val b = event.timeSecs >= previous.timeSecs
      report(b, "validTimeSequence failed", event, previousEvent)
    }
    case None => event.timeSecs >= 0
  }

  def errorsIn(events: List[MatchEvent]): Int = {
    @tailrec
    def loop(events: List[MatchEvent], errorCount: Int): Int = events match {
      case Nil => errorCount
      case h :: Nil => countErrors(h, None) + errorCount
      case h :: t => loop(t, errorCount + countErrors(h, t.headOption))
    }
    loop(events, 0)
  }

  private def report(result: Boolean, msg: String, event: MatchEvent, previousEvent: Option[MatchEvent]): Boolean = {
    //if (!result) println(s"$msg: $event, $previousEvent")
    result
  }

  private def countErrors(event: MatchEvent): Int = {

    countErrors(event, None, List(validTeam, initialTotalAddsUp))
  }

  private def countErrors(event: MatchEvent, previousEvent: Option[MatchEvent]): Int = {

    countErrors(event, previousEvent, List(validTeam, totalsAddUp, validTimeSequence))
  }

  private def countErrors(event: MatchEvent, previousEvent: Option[MatchEvent], checks: List[Check]): Int = {

    checks.foldLeft(0)((errorCount, check) => errorCount + boolean2Int(check(event, previousEvent)))
  }

  private def boolean2Int(b: Boolean): Int = if (b) 0 else 1

  private def teamTotal(event: MatchEvent, team: Int): Int = if (team == 0) event.team1Total else event.team2Total

  private def teamPoints(event: MatchEvent, team: Int): Int = if (team == event.team) event.points else 0

  private def otherTeam(team: Int): Int = if (team == 0) 1 else 0
}
