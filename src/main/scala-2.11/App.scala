import akka.actor.{Actor, ActorRef, ActorSystem, Props}

case class GetState()

class MatchStateActor extends Actor {
  override def receive: Receive = {
    def matchState(state: MatchState): Receive = {
      case event: MatchEvent =>
        val newState = new MatchState(event :: state.rawEvents)
        context.become(matchState(newState))
      case GetState => sender ! state
    }
    matchState(new MatchState(List()))
  }
}

class TickerTapeMain extends Actor {

  val system = ActorSystem("MatchState")

  val matchStateActor: ActorRef = system.actorOf(Props[MatchStateActor], "matchState")

  val queryMatchAtPoint: Long => Unit = i => matchStateActor ! GetState

  val matchStateSubscription = TestData.dummyQueryInterval.subscribe(queryMatchAtPoint)

  val parseEventBits: Int => MatchEvent =
    event => MatchEvent.parseInt(event)

  val onNext: MatchEvent => Unit =
    event => matchStateActor ! event

  val onError: Throwable => Unit =
    t => Unit

  val onComplete: () => Unit =
    () => {
      println("End of game data")
      matchStateSubscription.unsubscribe()
      context.stop(self)
      System.exit(0)
    }

  TestData.sample2Observable.map(parseEventBits).subscribe(onNext, onError, onComplete)


  override def receive: Receive = {
    /*m.rawEvents.headOption.foreach(println)*/
    case m: MatchState => m.cleanedEvents.headOption.foreach(event => println(MatchEvent.describe(event)))
  }

  def describe: Option[MatchEvent] => Option[String] = lift(MatchEvent.describe)
  def lift[A, B](f: A => B): Option[A] => Option[B] = _ map f
}
