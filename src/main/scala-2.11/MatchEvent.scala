

case class MatchEvent(points: Int, team: Int, team2Total: Int, team1Total: Int, timeSecs: Int)

object MatchEvent {

  val pointsMask = 0x3
  val teamMask = 0x4
  val team2TotalMask = 0x7F8
  val team1TotalMask = 0x7F800
  val timeSecsMask = 0x7FF80000

  def parseInt(event: Int): MatchEvent = {

    MatchEvent(
      (event & pointsMask) >>> 0,
      (event & teamMask) >>> 2,
      (event & team2TotalMask) >>> 3,
      (event & team1TotalMask) >>> 11,
      (event & timeSecsMask) >>> 19)
  }

  def toInt(event: MatchEvent): Int = {

    val points = event.points << 0
    val team = event.team << 2
    val team2Total = event.team2Total << 3
    val team1Total = event.team1Total << 11
    val timeSecs = event.timeSecs << 19

    timeSecs | team1Total | team2Total | team | points
  }

  def describe(event: MatchEvent): String = {
    s"At ${mmss(event.timeSecs)}, ${teamName(event.team)} " +
    f"scored ${event.points}, making it ${event.team1Total}%03d vs. ${event.team2Total}%03d"
  }

  private def teamName(team: Int): String = {
    if (team == 0) "Team 1" else "Team 2"
  }
  private def mmss(secs: Int): String = {
    f"${secs / 60}%02d:${secs % 60}%02d"
  }
}


