import Fixes._

class MatchState(val rawEvents: List[MatchEvent]) {

  val cleanedEvents = clean(rawEvents)
}
