name := "img-test"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
  "io.reactivex" %% "rxscala" % "0.25.0",
  "com.typesafe.akka" %% "akka-actor" % "2.3.11"
)